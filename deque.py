from collections import deque

d=deque()
d.append(1)
d.append(4)
d.append(4)
d.appendleft(5)
d.popleft()
d.extend([56])
d.extendleft([8,7,6,9])
d.rotate(1)
print(d)